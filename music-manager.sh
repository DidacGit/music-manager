#!/bin/bash
# Disable bash's POSIX mode
# set +o posix

# build-playlist: Build playlists for each artist of the library. These contain the most played songs of each artist.
# requires the package 'id3v2'

PROGNAME="$(basename $0)"

usage () {
    cat <<- _EOF_
    $PROGNAME: usage: $PROGNAME [-h] [-l DIR] [-a DIR] [DIR]

    Manage library and build playlists for each artist in the library. The tree should be: library -> artists -> albums.
    Albums only contain music files and one cover art image.

    1) Check the library or artist structure. They should contain only
    directories, and album directories should contain only audio files
    and one image file (the album cover).

    	1) Empty files or directories.
	2) Non-audio and non-image files.
	3) Music files where they shouldn't be. They should only be in the third level of depth.
	4) Directories where they shouldn't be. They should only be in the first and second level of depth.
	5) More than one image file in the third level of depth.

    1.2) Convert all the ID3 v1 tags into v2

    1.3) Track number as (e.g.) '2/12' or just '2'?

    3) Check metadata. Every song of an artist should have the same
    'artist'. Every song in an album should have the same 'album' and
    'year'. And that it has 'track number' and 'title'.

    4) Check that the files and folders names correspond the
    metadata. The artist folder should equal 'artist'. The album
    folder should equal 'year' - 'album'. The song files should equal
    'tracknum' - 'title'.

    TODO) Make every artist song also have the same 'genre' and
    manage playlists using these

    TODO) Make it ignore the "Live" albums

    4) 
    
    positional parameters:
      DIR                  Directory path of the library.

    optional arguments:
      -h, --help	   Show this help message and exit.
      -l, --library DIR	   Build playlists for each artist in the library. This is the default behavior.
      -a, --artist DIR	   Build a playlist only for the selected artist directory.
_EOF_
    # echo "$PROGNAME: usage: $PROGNAME [-h] [-a DIR] [-l DIR] [DIR]" >&2
    # exit 1
}

MUSIC_FORMATS='.*mp3\|.*flac'
IMAGE_FORMATS='.*jpg\|.*png'
MUSIC_IMAGE_FORMATS='\('"$MUSIC_FORMATS"'\|'"$IMAGE_FORMATS"'\)'

echo "Error List:

" > errors.tmp
# All the real song files
find -mindepth 3 -maxdepth 3 -type f ! -empty -regex "$MUSIC_FORMATS" > songs.tmp

get_tag () {
    local tag SONG 
    tag="$1"
    SONG="$2"
    case "$tag" in
	artist) tag=TPE1
		;;
	album) tag=TALB
	       ;;
	year) tag=TYER
	      ;;
	name) tag=TIT2
	      ;;
	number) tag=TRCK
		;;
    esac
    # The '-R' option only works with id3v2 tags (not v1)
    id3v2 -R "$SONG" | grep -E "^$tag" | sed -E '1s/^.*?: (.*?$)/\1 /'
}

has_id3v2_tags () {
    local SONG="$1"
    for tag in artist album year name number; do
	tag="$(get_tag "$tag" "$SONG")"
	if [ -z "$tag" ]; then
	    return 1
	fi
    done
    return 0
}

# add_id3v2_tag_error () {
#     local SONG="$1"
#     echo "V2 Tags not found:          $SONG" >> errors.tmp    
# }

check_tags () {
    local SONG="$1"
    if $(has_id3v2_tags "$SONG"); then
	echo "Has tags"
    else
	# convert from v1 to v2
	id3v2 -C "$SONG" &> /dev/null
	if $(has_id3v2_tags "$SONG"); then
	    echo "Has tags"
	fi
	echo "Doesn't have tags"
    fi
}

do_structure () {
    readarray -t ARTISTS < <(awk -F '/' '{ print $2 }' songs.tmp | sort | uniq)
    for artist in "${ARTISTS[@]}"; do
    	echo "ARTIST: $artist"
	readarray -t ALBUMS < <(grep -F "/${artist}/" songs.tmp | awk -F '/' '{ print $3 }' | sort | uniq)
    	for album in "${ALBUMS[@]}"; do
    	    echo "ALBUM: $album"
	    readarray -t SONGS < <(grep -F "/${artist}/${album}" songs.tmp | awk -F '/' '{ print $4 }' | sort | uniq)
	    for song in "${SONGS[@]}"; do
		echo "$song"
	    done
    	done
    done
}

check_tags "$1"

cat errors.tmp

rm songs.tmp
rm errors.tmp
